#!/usr/bin/env bash
DATE=`date +%Y-%m-%d`
DRY_RUN=0

neige_remote="neige"
owncloud_tar="latest-21.tar.bz2"
owncloud_local="nextcloud"
ttrss_local="tt-rss"
rainloop_local="rainloop"
owncloud_remote="clownd"
ttrss_remote="tt-rss"
rainloop_remote="rainloop"
owncloud_version="s/\\$OC_VersionString = '\(.*\)';/\1/p"
rainloop_version="s/.*APP_VERSION', '\(.*\)');/\1/p"
remote_host=

help() {
    echo "Usage: upgrade [--dry-run]"
    echo "Options:"
    echo "    -d, --dry-run       Do not upgrade."
    exit 2
}

version() {
  echo "$@" | gawk -F. '{ printf("%03d%03d%03d\n", $1,$2,$3); }';
}

update_dir() {
    echo "Upgrading $2 from $4 to $3 ..."
    if [ $DRY_RUN -eq 0 ]; then
        sshpass -e psftp $remote_host << !
   cd $neige_remote
   put -r $1 $2-new
   mv $2 $2-$DATE
   mv $2-new $2
!
    fi
    echo "Done"
}

do_update() {
    touch do_update
    sshpass -e psftp $remote_host << !
   cd $neige_remote
   put do_update
!
}

prepare_owncloud() {
    sshpass -e psftp $remote_host << !
   get $neige_remote/$owncloud_remote/config/config.php
   get $neige_remote/$owncloud_remote/version.php
!

    remote_version=`sed -n "s/\\$OC_VersionString = '\(.*\)';/\1/p" version.php`
    wget -O ${owncloud_tar} https://download.nextcloud.com/server/releases/${owncloud_tar}
    wget -O ${owncloud_tar}.md5 https://download.nextcloud.com/server/releases/${owncloud_tar}.md5
    md5sum -c ${owncloud_tar}.md5
    tar xjf $owncloud_tar
    cp config.php $owncloud_local/config/
    local_version=`sed -n "s/\\$OC_VersionString = '\(.*\)';/\1/p" $owncloud_local/version.php`

    # Check versions.
    if [ "$(version "$remote_version")" -ge "$(version "$local_version")" ]; then
        return 0
    fi

    # Do upgrade.
    update_dir $owncloud_local $owncloud_remote $local_version $remote_version
    do_update
}

prepare_ttrss () {
    # Retrieve .git and get versions.
    mkdir -p $ttrss_local
    cd $ttrss_local
    sshpass -e psftp $remote_host << !
   get -r $neige_remote/$ttrss_remote/.git
!
    remote_version=`git rev-parse HEAD`
    git reset --hard
    git pull origin master
    local_version=`git rev-parse HEAD`
    cd -
    rm -rf $ttrss_local

    # Check versions.
    if [ "$(version "$remote_version")" -ge "$(version "$local_version")" ]; then
        return 0
    fi

    # Retrieve remote working copy and upgrade.
    sshpass -e psftp $remote_host << !
   get -r $neige_remote/$ttrss_remote
!
    cd $ttrss_local
    git reset --hard
    git pull origin master
    cd -
    update_dir $ttrss_local $ttrss_remote $local_version $remote_version
}

prepare_rainloop () {
    # Retrieve index.php to get remote version.
    sshpass -e psftp $remote_host << !
   get -r $neige_remote/$rainloop_remote/index.php
!

    remote_version=`sed -n "s/.*APP_VERSION', '\(.*\)');/\1/p" index.php`

    # Download last rainloop version.
    wget -O rainloop-community-latest.zip https://www.rainloop.net/repository/webmail/rainloop-community-latest.zip
    wget -O rainloop-community-latest.zip.asc https://www.rainloop.net/repository/webmail/rainloop-community-latest.zip.asc
    wget -O RainLoop.asc https://www.rainloop.net/repository/RainLoop.asc
    gpg --import RainLoop.asc
    gpg --verify  rainloop-community-latest.zip.asc rainloop-community-latest.zip
    unzip -o rainloop-community-latest.zip "index.php" -d $rainloop_local
    local_version=`sed -n "s/.*APP_VERSION', '\(.*\)');/\1/p" $rainloop_local/index.php`

    # Check versions.
    if [ "$(version "$remote_version")" -ge "$(version "$local_version")" ]; then
        return 0
    fi

    # Retrieve full rainloop and upgrade.
    sshpass -e psftp $remote_host << !
   get -r $neige_remote/$rainloop_remote
!
    unzip -o rainloop-community-latest.zip -d $rainloop_local
    update_dir $rainloop_local $rainloop_remote $local_version $remote_version
}

while [ $# -gt 0 ]
do
i="$1"
case $i in
    -d|--dry-run)
    DRY_RUN=1
    shift
    ;;
    -h|--help)
    help
    ;;
    *)
    remote_host=$i
    shift
    ;;
esac
done

if [ $DRY_RUN -eq 1 ]; then
    echo "WARNING: This is a dry run, no modification will be applied."
fi

SSHPASS=$(secret-tool lookup $neige_remote $remote_host)
if [ -z "$SSHPASS" ]; then
    echo "Enter credentials for $remote_host"
    secret-tool store --label "$neige_remote with $remote_host" $neige_remote $remote_host
    SSHPASS=$(secret-tool lookup $neige_remote $remote_host)
fi
export SSHPASS

# exit when any command fails
set -e

prepare_owncloud
prepare_ttrss
prepare_rainloop

exit 0
