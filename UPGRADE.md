Update TT-RSS
=============

* log out tt-rss
* download tt-rss from the server
* git reset --hard (optional)
* git pull origin master
* rename tt-rss into tt-rss-YYMMDD
* upload tt-rss back to the server
* connect as admin to perform update
* ensure execution right to update_feed.sh

Update Clownd
=============

* download the last owncloud version
* copy config.php from the server
* upload owncloud-A.B.C
* rename clownd into clownd-X.Y.Z
* rename owncloud-A.B.C into clownd
* touch do_update
* check integrity by enabling it in config.php
