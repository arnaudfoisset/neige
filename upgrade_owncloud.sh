#!/bin/sh

if [ ! -f "/homez.115/letourdump/neige/do_update" ]; then exit 0; fi

echo "Running clownd update..."
/usr/local/php7.4/bin/php /homez.115/letourdump/neige/clownd/occ maintenance:mode --on
/usr/local/php7.4/bin/php /homez.115/letourdump/neige/clownd/occ upgrade
/usr/local/php7.4/bin/php /homez.115/letourdump/neige/clownd/occ maintenance:mode --off

rm "/homez.115/letourdump/neige/do_update"

